﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;

[Serializable]public class readjson
    {
        public int id;
        public string name;
        public string dest;
        public string desc;
        public string comment;
        public string img;
    };

[Serializable]public class listOfDetails
    {
        public readjson[] value;
    };

public class Footer : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]  private Image newsfeed;
    [SerializeField]  private Image profile;
    [SerializeField]  private Button homeButton;
    [SerializeField]  private Button profileButton;
    [SerializeField]  private GameObject content;
    [SerializeField]  private GameObject post;




    void OnEnable()
    {
        newsfeed.gameObject.SetActive(false);
        profileButton.interactable=(false);
        profileButton.transform.GetChild(1).gameObject.SetActive(true);
        string json=File.ReadAllText("Assets/valueOfPost.json");
        listOfDetails readeddata=JsonUtility.FromJson<listOfDetails>(json);
        for(int i=0;i<readeddata.value.Length;i++)
        {
            var newpost=Instantiate(post,content.transform);
            var scpt=newpost.GetComponent<PostLikeButtonScript>();
            scpt.setValuesToPost(readeddata.value[i]);
        }
    }
    public void onClick(bool state)
    {
        profile.gameObject.SetActive(state);
        newsfeed.gameObject.SetActive(!state);
        homeButton.interactable=state;
        homeButton.transform.GetChild(0).gameObject.SetActive(!state);
        profileButton.interactable=(!state);
        profileButton.transform.GetChild(1).gameObject.SetActive(state);        
    }
}
