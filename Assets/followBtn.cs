﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class followBtn : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Button follow;
    [SerializeField] private Button unfollow;
    
    public void Onclick(bool state)
    {
        unfollow.gameObject.SetActive(!state);
        follow.gameObject.SetActive(state);
    }
}
