﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;

[Serializable]public class savecomment
{
    public int id;
    public string com;
};
[Serializable]public class listOfComments
{
    public List<savecomment> lstcom=new List<savecomment>(); 
};

public class PostLikeButtonScript : MonoBehaviour
{

//method to values as parameter set

    // Start is called before the first frame update
    
    [SerializeField]  private Button Like;
    [SerializeField]  private Text usercomment;
    [SerializeField]  private InputField userInput;
    [SerializeField]  private GameObject commentScreen;
    [SerializeField]  private GameObject postScreen;
    [SerializeField]  private Image postpic;
    [SerializeField]  private Image profilepic;
    [SerializeField]  private Image commentpic;

    public Text name;
    public Text dest;
    public Text desc;

    public bool state=false;
    public bool doubletapstate=false;
    public static int count=0;
    public double time1;
    public double time2;
    public listOfComments saveCommentlist;
    public savecomment obj;

    public int sno;

    public void setValuesToPost(readjson obj)
    {
        sno=obj.id;
        profilepic.sprite=Resources.Load<Sprite>(obj.img);
        commentpic.sprite=Resources.Load<Sprite>(obj.img);
        name.text=obj.name;
        dest.text=obj.dest;
        desc.text=obj.desc;
        //GetInput(obj.comment);
        postpic.sprite=Resources.Load<Sprite>(obj.img);
    }
    public void likebutton()
    {
        Like.transform.GetChild(0).gameObject.SetActive(state);
        state=!state;
        doubletapstate=!doubletapstate;
    }
    public void doubletap()
    {
        if(count==0)
        {
            count++;
            time1=Time.time;
        }
        else if(count==1)
        {
            time2=Time.time;
            if(time2-time1 < 0.5)
            {
                if(doubletapstate==false)
                {
                    likebutton();
                    doubletapstate=true;
                }
                else
                {
                    return ;
                }
            }
            else
            {
                count=1;
                time1=time2;
            }
        }
    }
    public string print(string s)
    {
        string temp;
        temp="<color=#003569>antonigracia  </color><color=#353535> "+s+"</color>";
        return temp;
    }
    public void printcomment(string s)
    {
        if(!string.IsNullOrEmpty(s))
        {
            var newcomment=Instantiate(usercomment,commentScreen.transform); 
            newcomment.text=print(s);
            newcomment.gameObject.SetActive(true);            
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)commentScreen.transform);
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)postScreen.transform);
        }
    }
    // public void save(string s)
    // {
    //     saveCommentlist.lstcom.Add(new savecomment());
    //     obj.id=sno;
    //     obj.com=s;
    //     saveCommentlist.lstcom.Add(obj);
    //     var temp=JsonUtility.ToJson(saveCommentlist);
    //     System.IO.File.WriteAllText("Assets/SavedComments.json", temp);
    // }
    public void GetInput(string s)
    {
        printcomment(s);
        //save(s);
        userInput.text="";
        s="";
    }
}

